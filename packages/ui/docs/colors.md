CSS variables are used to define the theme's color scheme.

## Brand colors

`--color-primary`

```js
<div style={{ color: 'var(--color-primary)' }}>
{faker.lorem.sentence(5)}
</div>
```

## Colors for interactions

`--color-danger`

```js
<div style={{ color: 'var(--color-danger)' }}>
{faker.lorem.sentence(5)}
</div>
```

`--color-valid`

```js
<div style={{ color: 'var(--color-valid)' }}>
{faker.lorem.sentence(5)}
</div>
```

`--color-warning`

```js
<div style={{ color: 'var(--color-warning)' }}>
{faker.lorem.sentence(5)}
</div>
```

`--color-pending`

```js
<div style={{ color: 'var(--color-pending)' }}>
{faker.lorem.sentence(5)}
</div>
```
