An icon, from the [Feather](https://feathericons.com/) icon set.

```js
<Icon>arrow_right</Icon>
```

The color can be changed.

```js
<Icon color="red">arrow_right</Icon>
``` 

The size can be changed.

```js
<Icon size={48}>arrow_right</Icon>
``` 
